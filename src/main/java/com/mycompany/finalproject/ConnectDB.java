/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalproject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author optimuspnj
 */
public class ConnectDB {
    
    String driverMsg = "",driverErr = "",loginMsg = "",loginErr = "";
    
    public Connection MyDBConnection (String inuser, String inpass) {
        String url = "jdbc:mysql://localhost:3306/Tecmis";
        String user = inuser;
        String pass = inpass;
        Connection connection = null;
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            driverMsg = "Driver loaded...\n";
            connection = DriverManager.getConnection(url,user,pass);
            loginMsg = "Connected!\n";
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
            driverErr = "Driver Error!\n";
        } catch (SQLException ex) {
            loginErr = "User name or password incorrect!\n";
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
}
