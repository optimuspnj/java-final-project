/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalproject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author optimuspnj
 */
public class GPACalculator {
    Connection conn = null;
    
    private static DecimalFormat df = new DecimalFormat("0.00");
    
    public String getGpa (String stID) {
        
        ConnectDB myConnectDb = new ConnectDB();
        conn = myConnectDb.MyDBConnection("lecturer", "lecpwd");
        double sgpa = 0;
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT exam_score FROM `exam_marks` WHERE exam_marks.exam_st_id = '"+stID+"' AND exam_marks.exam_type = 'end' ");
            int i = 1;
            double sum = 0;
            while (rs.next()) {
                int marks = rs.getInt("exam_score");
                double gpa = 0;
                if (marks >= 90) {
                    gpa = 4.0;
                }
                else if (marks > 85 && marks < 89) {
                    gpa = 4.0;
                }
                else if (marks > 80 && marks < 84) {
                    gpa = 3.7;
                }
                else if (marks > 75 && marks < 79) {
                    gpa = 3.3;
                }
                else if (marks > 70 && marks < 74) {
                    gpa = 3.0;
                }
                else if (marks > 65 && marks < 69) {
                    gpa = 2.7;
                }
                else if (marks > 60 && marks < 64) {
                    gpa = 2.3;
                }
                else if (marks > 55 && marks < 59) {
                    gpa = 2.0;
                }
                else if (marks > 50 && marks < 54) {
                    gpa = 1.7;
                }
                else if (marks > 45 && marks < 49) {
                    gpa = 1.3;
                }
                else if (marks > 40 && marks < 44) {
                    gpa = 1.0;
                }
                else if (marks < 40) {
                    gpa = 0.0;
                }
                sum += gpa;
                i++;
            }
            sgpa = (sum/i);
            
        } catch (SQLException ex) {
            System.out.println("Error retrieving data!");
            Logger.getLogger(AdminLoginInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
        return df.format(sgpa);
    }
}
