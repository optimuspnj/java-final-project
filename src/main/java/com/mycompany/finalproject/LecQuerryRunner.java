/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalproject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author optimuspnj
 */
public class LecQuerryRunner {
    String status = null;
    String driverMsg = null;
    String loginMsg = null;
    String driverErr = null;
    String loginErr = null;
    public void runQuerry (String sql) {
        
        Connection conn;
    
        ConnectDB myConnectDb = new ConnectDB();
        conn = myConnectDb.MyDBConnection("lecturer", "lecpwd");
        
        driverMsg = myConnectDb.driverMsg;
        loginMsg = myConnectDb.loginMsg;
        driverErr = myConnectDb.driverErr;
        loginErr = myConnectDb.loginErr;    
        
        try {
            Statement stmt = conn.createStatement();
            
            stmt.executeUpdate(sql);
            
            
            status = "Querry successfull\n";
            } catch (SQLException ex) {
                status = "Querry failed!\n";
                Logger.getLogger(AdminCreateUser.class.getName()).log(Level.SEVERE, null, ex);
            }
    
    }
}
